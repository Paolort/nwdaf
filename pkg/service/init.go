package service

import (
	"bufio"
	"fmt"
	aperLogger "github.com/free5gc/aper/logger"
	nasLogger "github.com/free5gc/nas/logger"
	ngapLogger "github.com/free5gc/ngap/logger"
	"github.com/free5gc/util/httpwrapper"
	logger_util "github.com/free5gc/util/logger"
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"gitlab.com/Paolort/nwdaf/internal/collections"
	"gitlab.com/Paolort/nwdaf/internal/context"
	"gitlab.com/Paolort/nwdaf/internal/dataCollectors"
	"gitlab.com/Paolort/nwdaf/internal/logger"
	"gitlab.com/Paolort/nwdaf/internal/sbi/event_report"
	"gitlab.com/Paolort/nwdaf/internal/sbi/metrics"
	"gitlab.com/Paolort/nwdaf/internal/util"
	"gitlab.com/Paolort/nwdaf/pkg/factory"
	"os"
	"os/exec"
	"os/signal"
	"runtime/debug"
	"sync"
	"syscall"

	"github.com/free5gc/openapi/models"
	fsmLogger "github.com/free5gc/util/fsm/logger"
	"github.com/gin-contrib/cors"
	"gitlab.com/Paolort/nwdaf/internal/sbi/consumer"
)

type NWDAF struct {
	KeyLogPath string
}

type (
	// Commands information.
	Commands struct {
		config string
	}
)

var commands Commands

var cliCmd = []cli.Flag{
	cli.StringFlag{
		Name:  "config, c",
		Usage: "Load configuration from `FILE`",
	},
	cli.StringFlag{
		Name:  "log, l",
		Usage: "Output NF log to `FILE`",
	},
	cli.StringFlag{
		Name:  "log5gc, lc",
		Usage: "Output free5gc log to `FILE`",
	},
}

func (*NWDAF) GetCliCmd() (flags []cli.Flag) {
	return cliCmd
}

func (nwdaf *NWDAF) Initialize(c *cli.Context) error {
	commands = Commands{
		config: c.String("config"),
	}

	if commands.config != "" {
		if err := factory.InitConfigFactory(commands.config); err != nil {
			return err
		}
	} else {
		if err := factory.InitConfigFactory(util.NwdafDefaultConfigPath); err != nil {
			return err
		}
	}

	if err := factory.CheckConfigVersion(); err != nil {
		return err
	}

	if _, err := factory.NwdafConfig.Validate(); err != nil {
		return err
	}

	nwdaf.SetLogLevel()

	return nil
}

func (nwdaf *NWDAF) SetLogLevel() {
	if factory.NwdafConfig.Logger == nil {
		logger.InitLog.Warnln("NWDAF config without log level setting!!!")
		return
	}

	if factory.NwdafConfig.Logger.AMF != nil {
		if factory.NwdafConfig.Logger.AMF.DebugLevel != "" {
			if level, err := logrus.ParseLevel(factory.NwdafConfig.Logger.AMF.DebugLevel); err != nil {
				logger.InitLog.Warnf("NWDAF Log level [%s] is invalid, set to [info] level",
					factory.NwdafConfig.Logger.AMF.DebugLevel)
				logger.SetLogLevel(logrus.InfoLevel)
			} else {
				logger.InitLog.Infof("NWDAF Log level is set to [%s] level", level)
				logger.SetLogLevel(level)
			}
		} else {
			logger.InitLog.Warnln("NWDAF Log level not set. Default set to [info] level")
			logger.SetLogLevel(logrus.InfoLevel)
		}
		logger.SetReportCaller(factory.NwdafConfig.Logger.AMF.ReportCaller)
	}

	if factory.NwdafConfig.Logger.NAS != nil {
		if factory.NwdafConfig.Logger.NAS.DebugLevel != "" {
			if level, err := logrus.ParseLevel(factory.NwdafConfig.Logger.NAS.DebugLevel); err != nil {
				nasLogger.NasLog.Warnf("NAS Log level [%s] is invalid, set to [info] level",
					factory.NwdafConfig.Logger.NAS.DebugLevel)
				logger.SetLogLevel(logrus.InfoLevel)
			} else {
				nasLogger.SetLogLevel(level)
			}
		} else {
			nasLogger.NasLog.Warnln("NAS Log level not set. Default set to [info] level")
			nasLogger.SetLogLevel(logrus.InfoLevel)
		}
		nasLogger.SetReportCaller(factory.NwdafConfig.Logger.NAS.ReportCaller)
	}

	if factory.NwdafConfig.Logger.NGAP != nil {
		if factory.NwdafConfig.Logger.NGAP.DebugLevel != "" {
			if level, err := logrus.ParseLevel(factory.NwdafConfig.Logger.NGAP.DebugLevel); err != nil {
				ngapLogger.NgapLog.Warnf("NGAP Log level [%s] is invalid, set to [info] level",
					factory.NwdafConfig.Logger.NGAP.DebugLevel)
				ngapLogger.SetLogLevel(logrus.InfoLevel)
			} else {
				ngapLogger.SetLogLevel(level)
			}
		} else {
			ngapLogger.NgapLog.Warnln("NGAP Log level not set. Default set to [info] level")
			ngapLogger.SetLogLevel(logrus.InfoLevel)
		}
		ngapLogger.SetReportCaller(factory.NwdafConfig.Logger.NGAP.ReportCaller)
	}

	if factory.NwdafConfig.Logger.FSM != nil {
		if factory.NwdafConfig.Logger.FSM.DebugLevel != "" {
			if level, err := logrus.ParseLevel(factory.NwdafConfig.Logger.FSM.DebugLevel); err != nil {
				fsmLogger.FsmLog.Warnf("FSM Log level [%s] is invalid, set to [info] level",
					factory.NwdafConfig.Logger.FSM.DebugLevel)
				fsmLogger.SetLogLevel(logrus.InfoLevel)
			} else {
				fsmLogger.SetLogLevel(level)
			}
		} else {
			fsmLogger.FsmLog.Warnln("FSM Log level not set. Default set to [info] level")
			fsmLogger.SetLogLevel(logrus.InfoLevel)
		}
		fsmLogger.SetReportCaller(factory.NwdafConfig.Logger.FSM.ReportCaller)
	}

	if factory.NwdafConfig.Logger.Aper != nil {
		if factory.NwdafConfig.Logger.Aper.DebugLevel != "" {
			if level, err := logrus.ParseLevel(factory.NwdafConfig.Logger.Aper.DebugLevel); err != nil {
				aperLogger.AperLog.Warnf("Aper Log level [%s] is invalid, set to [info] level",
					factory.NwdafConfig.Logger.Aper.DebugLevel)
				aperLogger.SetLogLevel(logrus.InfoLevel)
			} else {
				aperLogger.SetLogLevel(level)
			}
		} else {
			aperLogger.AperLog.Warnln("Aper Log level not set. Default set to [info] level")
			aperLogger.SetLogLevel(logrus.InfoLevel)
		}
		aperLogger.SetReportCaller(factory.NwdafConfig.Logger.Aper.ReportCaller)
	}
}

func (nwdaf *NWDAF) FilterCli(c *cli.Context) (args []string) {
	for _, flag := range nwdaf.GetCliCmd() {
		name := flag.GetName()
		value := fmt.Sprint(c.Generic(name))
		if value == "" {
			continue
		}

		args = append(args, "--"+name, value)
	}
	return args
}

func (nwdaf *NWDAF) Start() {
	logger.InitLog.Infoln("Nwdaf started")

	router := logger_util.NewGinWithLogrus(logger.GinLog)
	router.Use(cors.New(cors.Config{
		AllowMethods: []string{"GET", "POST", "OPTIONS", "PUT", "PATCH", "DELETE"},
		AllowHeaders: []string{
			"Origin", "Content-Length", "Content-Type", "User-Agent", "Referrer", "Host",
			"Token", "X-Requested-With",
		},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		AllowAllOrigins:  true,
		MaxAge:           86400,
	}))

	for _, serviceName := range factory.NwdafConfig.Configuration.ServiceNameList {
		switch metrics.MetricsServiceName(serviceName) {
		case metrics.MetricsServiceName_NWDAF:
			metrics.AddService(router)
		}
		switch event_report.SmfEventServiceName(serviceName) {
		case event_report.SmfEventService_NWDAF:
			event_report.AddService(router)
		}
	}

	self := context.NWDAF_Self()
	util.InitNwdafContext(self)

	// Register to NRF
	var profile models.NfProfile
	if profileTmp, err := consumer.BuildNFInstance(self); err != nil {
		logger.InitLog.Error("Build NWDAF Profile Error")
	} else {
		profile = profileTmp
	}

	if _, nfId, err := consumer.SendRegisterNFInstance(self.NrfUri, self.NfId, profile); err != nil {
		logger.InitLog.Warnf("Send Register NF Instance failed: %+v", err)
	} else {
		self.NfId = nfId
	}

	//Setup Exit point
	signalChannel := make(chan os.Signal, 1)
	signal.Notify(signalChannel, os.Interrupt, syscall.SIGTERM)
	go func() {
		defer func() {
			if p := recover(); p != nil {
				// Print stack for panic to log. Fatalf() will let program exit.
				logger.InitLog.Fatalf("panic: %v\n%s", p, string(debug.Stack()))
			}
		}()

		<-signalChannel
		nwdaf.Terminate()
		os.Exit(0)
	}()

	collections.InitializeCollections()
	for i := range collections.GetSmfCollection() {
		dataCollectors.GetDevicesInfoFromSmf(collections.GetSmfCollection()[i])
		break
	}

	pemPath := util.NwdafDefaultPemPath
	keyPath := util.NwdafDefaultKeyPath
	sbi := factory.NwdafConfig.Configuration.Sbi
	if sbi.Tls != nil {
		pemPath = sbi.Tls.Pem
		keyPath = sbi.Tls.Key
	}

	addr := fmt.Sprintf("%s:%d", self.BindingIPv4, self.SBIPort)
	server, err := httpwrapper.NewHttp2Server(addr, nwdaf.KeyLogPath, router)

	if server == nil {
		logger.InitLog.Errorf("Initialize HTTP server failed: %+v", err)
		return
	}

	if err != nil {
		logger.InitLog.Warnf("Initialize HTTP server: %+v", err)
	}

	serverScheme := factory.NwdafConfig.Configuration.Sbi.Scheme
	if serverScheme == "http" {
		err = server.ListenAndServe()
	} else if serverScheme == "https" {
		err = server.ListenAndServeTLS(pemPath, keyPath)
	}

	if err != nil {
		logger.InitLog.Fatalf("HTTP server setup failed: %+v", err)
	}
}

func (nwdaf *NWDAF) Exec(c *cli.Context) error {
	// NWDAF.Initialize(cfgPath, c)

	logger.InitLog.Traceln("args:", c.String("amfcfg"))
	args := nwdaf.FilterCli(c)
	logger.InitLog.Traceln("filter: ", args)
	command := exec.Command("./nwdaf", args...)

	stdout, err := command.StdoutPipe()
	if err != nil {
		logger.InitLog.Fatalln(err)
	}
	wg := sync.WaitGroup{}
	wg.Add(3)
	go func() {
		defer func() {
			if p := recover(); p != nil {
				// Print stack for panic to log. Fatalf() will let program exit.
				logger.InitLog.Fatalf("panic: %v\n%s", p, string(debug.Stack()))
			}
		}()

		in := bufio.NewScanner(stdout)
		for in.Scan() {
			fmt.Println(in.Text())
		}
		wg.Done()
	}()

	stderr, err := command.StderrPipe()
	if err != nil {
		logger.InitLog.Fatalln(err)
	}
	go func() {
		defer func() {
			if p := recover(); p != nil {
				// Print stack for panic to log. Fatalf() will let program exit.
				logger.InitLog.Fatalf("panic: %v\n%s", p, string(debug.Stack()))
			}
		}()

		in := bufio.NewScanner(stderr)
		for in.Scan() {
			fmt.Println(in.Text())
		}
		wg.Done()
	}()

	go func() {
		defer func() {
			if p := recover(); p != nil {
				// Print stack for panic to log. Fatalf() will let program exit.
				logger.InitLog.Fatalf("panic: %v\n%s", p, string(debug.Stack()))
			}
		}()

		if err = command.Start(); err != nil {
			logger.InitLog.Errorf("NWDAF Start error: %+v", err)
		}
		wg.Done()
	}()

	wg.Wait()

	return err
}

// Used in NWDAF planned removal procedure
func (nwdaf *NWDAF) Terminate() {
	logger.InitLog.Infof("Terminating NWDAF...")

	// deregister with NRF
	problemDetails, err := consumer.SendDeregisterNFInstance()
	if problemDetails != nil {
		logger.InitLog.Errorf("Deregister NF instance Failed Problem[%+v]", problemDetails)
	} else if err != nil {
		logger.InitLog.Errorf("Deregister NF instance Error[%+v]", err)
	} else {
		logger.InitLog.Infof("[NWDAF] Deregister from NRF successfully")
	}

	//callback.SendAmfStatusChangeNotify((string)(models.StatusChange_UNAVAILABLE), amfSelf.ServedGuamiList)
	logger.InitLog.Infof("NWDAF terminated")
}
