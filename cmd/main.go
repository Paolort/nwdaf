package main

import (
	"fmt"
	"github.com/asaskevich/govalidator"
	"gitlab.com/Paolort/nwdaf/internal/logger"
	"gitlab.com/Paolort/nwdaf/internal/util"
	"gitlab.com/Paolort/nwdaf/pkg/service"
	"path/filepath"

	"os"
	"runtime/debug"

	"github.com/urfave/cli"
)

var NWDAF = &service.NWDAF{}

func main() {
	defer func() {
		if p := recover(); p != nil {
			// Print stack for panic to log. Fatalf() will let program exit.
			logger.AppLog.Fatalf("panic: %v\n%s", p, string(debug.Stack()))
		}
	}()

	app := cli.NewApp()
	app.Name = "Nwdaf"
	app.Usage = "5G Network data analytics function (NWDAF)"
	app.Action = action
	app.Flags = NWDAF.GetCliCmd()
	if err := app.Run(os.Args); err != nil {
		logger.AppLog.Errorf("NWDAF Run error: %v\n", err)
		return
	}
}

func action(c *cli.Context) error {
	if err := initLogFile(c.String("log"), c.String("log5gc")); err != nil {
		logger.AppLog.Errorf("%+v", err)
		return err
	}

	if err := NWDAF.Initialize(c); err != nil {
		switch err1 := err.(type) {
		case govalidator.Errors:
			errs := err1.Errors()
			for _, e := range errs {
				logger.CfgLog.Errorf("%+v", e)
			}
		default:
			logger.CfgLog.Errorf("%+v", err)
		}

		logger.CfgLog.Errorf("[-- PLEASE REFER TO SAMPLE CONFIG FILE COMMENTS --]")
		return fmt.Errorf("Failed to initialize !!")
	}

	NWDAF.Start()

	return nil
}

func initLogFile(logNfPath, log5gcPath string) error {
	NWDAF.KeyLogPath = util.NwdafDefaultKeyLogPath

	if err := logger.LogFileHook(logNfPath, log5gcPath); err != nil {
		return err
	}

	if logNfPath != "" {
		nfDir, _ := filepath.Split(logNfPath)
		tmpDir := filepath.Join(nfDir, "key")
		if err := os.MkdirAll(tmpDir, 0o775); err != nil {
			logger.InitLog.Errorf("Make directory %s failed: %+v", tmpDir, err)
			return err
		}
		_, name := filepath.Split(util.NwdafDefaultKeyLogPath)
		NWDAF.KeyLogPath = filepath.Join(tmpDir, name)
	}

	return nil
}
