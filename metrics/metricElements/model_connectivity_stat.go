package metric_elements

import "gitlab.com/Paolort/nwdaf/metrics"

type ConnectivityStats struct {
	Connected3GPPUes    int
	Idle3GPPUes         int
	ConnectedNON3GPPUes int
	IdleNON3GPPUes      int
}

func InsertConnMetricInMap(connData ConnectivityStats) {
	metrics.AddMetric("free5gc_connected_3gpp_ues", float64(connData.Connected3GPPUes))
	metrics.AddMetric("free5gc_idle_3gpp_ues", float64(connData.Idle3GPPUes))
	metrics.AddMetric("free5gc_connected_NON_3gpp_ues", float64(connData.ConnectedNON3GPPUes))
	metrics.AddMetric("free5gc_idle_NON_3gpp_ues", float64(connData.IdleNON3GPPUes))
}
