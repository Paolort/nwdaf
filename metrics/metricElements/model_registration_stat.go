package metric_elements

import "gitlab.com/Paolort/nwdaf/metrics"

type RegistrationStats struct {
	Registered3GPPUes      int
	Deregistered3GPPUes    int
	RegisteredNON3GPPUes   int
	DeregisteredNON3GPPUes int
}

func InsertRegistationMetricInMap(regrData RegistrationStats) {
	metrics.AddMetric("free5gc_registered_3gpp_ues", float64(regrData.Registered3GPPUes))
	metrics.AddMetric("free5gc_deregistered_3gpp_ues", float64(regrData.Deregistered3GPPUes))
	metrics.AddMetric("free5gc_registered_NON_3gpp_ues", float64(regrData.RegisteredNON3GPPUes))
	metrics.AddMetric("free5gc_deregistered_NON_3gpp_ues", float64(regrData.DeregisteredNON3GPPUes))
}
