package metrics

import (
	"bytes"
	"fmt"
	"gitlab.com/Paolort/nwdaf/internal/logger"
)

type MetricElement struct {
	Name string
	Help string

	Value float64
}

var metricsMap map[string]MetricElement = map[string]MetricElement{}

func GetMetricsString() string {
	var toReturn bytes.Buffer
	for _, element := range metricsMap {
		//HELP
		toReturn.WriteString(fmt.Sprintf("# HELP %s %s\n", element.Name, element.Help))
		//TYPE
		toReturn.WriteString(fmt.Sprintf("# TYPE %s gauge\n", element.Name))
		//VALUE
		toReturn.WriteString(fmt.Sprintf("%s %f\n", element.Name, element.Value))
	}
	return toReturn.String()
}

func AddMetric(element MetricElement) {
	b := metricsMap //todo remove
	_ = b           //todo remove
	if !(&element == nil) {
		if !(element.Name == "") {
			metricsMap[element.Name] = element
		} else {
			logger.MetricLog.Errorf("Name of metric must be NOT null")
		}
	} else {
		logger.MetricLog.Errorf("Metric element is null")
	}

}
