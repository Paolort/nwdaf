package metric_elements

import (
	"fmt"
	"gitlab.com/Paolort/nwdaf/metrics"
)

type RegistrationStats struct {
	Entity                 string
	Registered3GPPUes      int
	Deregistered3GPPUes    int
	RegisteredNON3GPPUes   int
	DeregisteredNON3GPPUes int
}

func InsertRegistationMetricInMap(regrData RegistrationStats) {
	metrics.AddMetric(metrics.MetricElement{Name: fmt.Sprintf("free5gc_%s_registered_3gpp_ues", regrData.Entity), Help: "Number of registered 3GPP ues in the network", Value: float64(regrData.Registered3GPPUes)})
	metrics.AddMetric(metrics.MetricElement{Name: fmt.Sprintf("free5gc_%s_deregistered_3gpp_ues", regrData.Entity), Help: "Number of deregistered 3GPP ues in the network", Value: float64(regrData.Deregistered3GPPUes)})
	metrics.AddMetric(metrics.MetricElement{Name: fmt.Sprintf("free5gc_%s_registered_NON_3gpp_ues", regrData.Entity), Help: "Number of registered NON 3GPP ues in the network", Value: float64(regrData.RegisteredNON3GPPUes)})
	metrics.AddMetric(metrics.MetricElement{Name: fmt.Sprintf("free5gc_%s_deregistered_NON_3gpp_ues", regrData.Entity), Help: "Number of deregistered NON 3GPP ues in the network", Value: float64(regrData.DeregisteredNON3GPPUes)})
}

func ResetGlobalRegistrStat(regrStat RegistrationStats) {
	regrStat.Entity = ""
	regrStat.Registered3GPPUes = 0
	regrStat.Deregistered3GPPUes = 0
	regrStat.RegisteredNON3GPPUes = 0
	regrStat.DeregisteredNON3GPPUes = 0
}
