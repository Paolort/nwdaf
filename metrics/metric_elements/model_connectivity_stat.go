package metric_elements

import (
	"fmt"
	"gitlab.com/Paolort/nwdaf/metrics"
)

type ConnectivityStats struct {
	Entity              string //Name of the entity to witch devices are connected to
	Connected3GPPUes    int
	Idle3GPPUes         int
	ConnectedNON3GPPUes int
	IdleNON3GPPUes      int
}

func InsertConnMetricInMap(connData ConnectivityStats) {
	metrics.AddMetric(metrics.MetricElement{Name: fmt.Sprintf("free5gc_%s_connected_3gpp_ues", connData.Entity), Help: "Number of connected 3GPP ues in the network", Value: float64(connData.Connected3GPPUes)})
	metrics.AddMetric(metrics.MetricElement{Name: fmt.Sprintf("free5gc_%s_idle_3gpp_ues", connData.Entity), Help: "Number of idle 3GPP ues in the network", Value: float64(connData.Idle3GPPUes)})
	metrics.AddMetric(metrics.MetricElement{Name: fmt.Sprintf("free5gc_%s_connected_NON_3gpp_ues", connData.Entity), Help: "Number of connected NON 3GPP ues in the network", Value: float64(connData.ConnectedNON3GPPUes)})
	metrics.AddMetric(metrics.MetricElement{Name: fmt.Sprintf("free5gc_%s_idle_NON_3gpp_ues", connData.Entity), Help: "Number of idle NON 3GPP ues in the network", Value: float64(connData.IdleNON3GPPUes)})
}

func ResetGlobalConnStat(connData ConnectivityStats) {
	connData.Entity = ""
	connData.ConnectedNON3GPPUes = 0
	connData.IdleNON3GPPUes = 0
	connData.Connected3GPPUes = 0
	connData.Idle3GPPUes = 0
}
