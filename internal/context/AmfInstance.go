package context

import (
	"github.com/free5gc/openapi/models"
	"time"
)

type AmfInstance struct {
	Profile        models.NfProfile
	CustomName     string
	RetrivalTime   time.Time
	ValidityPeriod int32
	ConnectedUes   []*string
}
