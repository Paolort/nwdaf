package context

import (
	"github.com/free5gc/openapi/models"
	"time"
)

type SmfInstance struct {
	Profile        models.NfProfile
	RetrivalTime   time.Time
	ValidityPeriod int32
}

/*
func getEventsSingleDevice() {

}

func getEventsAnyDevice() {

}

*/
