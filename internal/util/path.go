//go:build !debug
// +build !debug

package util

var (
	NwdafDefaultKeyLogPath = "./log/amfsslkey.log"
	NwdafDefaultPemPath    = "./config/TLS/amf.pem"
	NwdafDefaultKeyPath    = "./config/TLS/amf.key"
	NwdafDefaultConfigPath = "./config/nwdaf.yaml"
)
