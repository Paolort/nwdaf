package producer

import (
	"gitlab.com/Paolort/nwdaf/internal/collections"
	"gitlab.com/Paolort/nwdaf/internal/dataCollectors"
	"gitlab.com/Paolort/nwdaf/metrics"
)

func HandleGetMetrics() string {
	//Updating collection of NFs
	collections.UpdateCollections()

	//Collecting data from AMF: devices in AoT for each AMF and add the number to the metric system
	dataCollectors.PresenceInAoIAllAmfs()

	//Update info for each device connected to AMFs.
	dataCollectors.UpdateDevicesInfoAmfs()

	//Get the information in string form in order to answer to the request.
	return metrics.GetMetricsString()
}
