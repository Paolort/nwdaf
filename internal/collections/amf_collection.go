package collections

import (
	"gitlab.com/Paolort/nwdaf/internal/context"
	"gitlab.com/Paolort/nwdaf/metrics"
	"sync"
)

var mutexAmfColl = &sync.RWMutex{}
var amfCollection map[string]*context.AmfInstance

func GetAmfCollection() map[string]*context.AmfInstance {
	initAmfCollection()
	//todo remove expired istances
	return amfCollection
}

/*
If the collection is nil then it initialize it
*/
func initAmfCollection() {
	if amfCollection == nil {
		amfCollection = map[string]*context.AmfInstance{}
	}
}

// ResetAmfCollection
/*
Set to nil the collection
*/
func ResetAmfCollection() {
	amfCollection = nil
	UpdateAmfCollStatistics()
}

// AddAmfsToCollection
/*
Add elements to the collection of context.AmfInstance. If elements are already in there they are replaced and updated.
*/
func AddAmfsToCollection(toBeAdded []context.AmfInstance) {
	for i := range toBeAdded {
		amfCollection[toBeAdded[i].Profile.NfInstanceId] = &toBeAdded[i]
	}
	UpdateAmfCollStatistics()
}

//AddAmfToCollection
/*
Add single AMF to the collection of context.AmfInstance. If element us already in there it is replaced and updated.
*/
func AddAmfToCollection(toBeAdded context.AmfInstance) {
	amfCollection[toBeAdded.Profile.NfInstanceId] = &toBeAdded
	UpdateAmfCollStatistics()
}

//UpdateAmfCollStatistics
/*
Update the statistics
*/
func UpdateAmfCollStatistics() {
	metrics.AddMetric(metrics.MetricElement{Name: "free5gc_amf_instances", Help: "Number of SMF in the core", Value: float64(len(amfCollection))})
}
