package collections

import (
	"gitlab.com/Paolort/nwdaf/internal/context"
	"gitlab.com/Paolort/nwdaf/metrics"
)

var smfCollection map[string]context.SmfInstance

func GetSmfCollection() map[string]context.SmfInstance {
	initSmfCollection()

	//todo remove expired istances
	return smfCollection
}

/*
If the collection is nil then it initialize it
*/
func initSmfCollection() {
	if smfCollection == nil {
		smfCollection = map[string]context.SmfInstance{}
	}
}

//ResetSmfCollection
/*
Set to nil the collection
*/
func ResetSmfCollection() {
	smfCollection = nil
	UpdateSmfCollStatistics()
}

// AddSmfsToCollection
/*
Add elements to the collection of context.SmfInstance. If elements are already in there they are replaced and updated.
*/
func AddSmfsToCollection(toBeAdded []context.SmfInstance) {
	for i := range toBeAdded {
		smfCollection[toBeAdded[i].Profile.NfInstanceId] = toBeAdded[i]
	}
	UpdateSmfCollStatistics()
}

//AddSmfToCollection
/*
Add single SMF to the collection of context.SmfInstance. If element us already in there it is replaced and updated.
*/
func AddSmfToCollection(toBeAdded context.SmfInstance) {
	smfCollection[toBeAdded.Profile.NfInstanceId] = toBeAdded
	UpdateAmfCollStatistics()
}

func UpdateSmfCollStatistics() {
	metrics.AddMetric(metrics.MetricElement{Name: "free5gc_smf_instances", Help: "Number of SMF in the core", Value: float64(len(smfCollection))})
}
