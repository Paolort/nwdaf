package collections

import (
	"fmt"
	"github.com/free5gc/openapi/Nnrf_NFDiscovery"
	"github.com/free5gc/openapi/models"
	"gitlab.com/Paolort/nwdaf/internal/sbi/consumer"
	"gitlab.com/Paolort/nwdaf/internal/util"
	"gitlab.com/Paolort/nwdaf/pkg/factory"
	"time"
)

var lastUpdate = time.Now()

const MIN_TIME_BEFORE_UPDATE_SECONDS = 180

// InitializeCollections
// Initialize collections for NFs, then it asks to NRF a list of NFs to put inside collections.
// After this function is possible to use NFs collections (that should have already members inside). It is also possible
// to initialize every type of NF collection by your self by calling the function inside a collection.
func InitializeCollections() {
	nrfUri := factory.NwdafConfig.Configuration.NrfUri
	initAmfCollection()
	initSmfCollection()
	lastUpdate = time.Now()

	//Add amf and smf instances to Amf and smf collection, asking to NRF
	AddAmfsToCollection(util.NfProfileToAmfInstances(RetrieveNfs(nrfUri, models.NfType_AMF), lastUpdate))
	AddSmfsToCollection(util.NfProfileToSmfInstances(RetrieveNfs(nrfUri, models.NfType_SMF), lastUpdate))
}

// UpdateCollections
// If minimum time after last update of NFs has elapsed, then collections are updated.
func UpdateCollections() {
	var timeDiff = (time.Now().Sub(lastUpdate)).Seconds()
	if timeDiff > MIN_TIME_BEFORE_UPDATE_SECONDS {
		InitializeCollections()
	}
}

// RetrieveNfs
// Returns the list of NF instances of the desired type. It needs the NRF uri.
func RetrieveNfs(nrfUri string, requestNfType models.NfType) models.SearchResult {
	//Inizializzazione parametri opzionali da passare alla richiesta
	param := Nnrf_NFDiscovery.SearchNFInstancesParamOpts{}

	//Richiesta a NRF per ottenere un'istanza di una NF del tipo requestNfType da parte di un NWDAF
	resp, err := consumer.SendSearchNFInstances(nrfUri, requestNfType, models.NfType_NWDAF, &param)

	if err != nil {
		errorA := fmt.Errorf("NWDAF can not select any NFs by NRF")
		fmt.Println(errorA)
		return models.SearchResult{}
	}

	return resp
}
