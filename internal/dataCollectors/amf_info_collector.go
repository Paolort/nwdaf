package dataCollectors

import (
	"fmt"
	"github.com/free5gc/openapi/Namf_EventExposure"
	"github.com/free5gc/openapi/models"
	"gitlab.com/Paolort/nwdaf/internal/collections"
	"gitlab.com/Paolort/nwdaf/internal/context"
	"gitlab.com/Paolort/nwdaf/metrics"
	"gitlab.com/Paolort/nwdaf/metrics/metric_elements"
	gocontext "golang.org/x/net/context"
)

// PresenceInAoIAllAmfs
// Asks to all amf, one by one, the list of connected UEs to that AMF. Add the number of connected UEs for each AMF
// to the metric system
func PresenceInAoIAllAmfs() {
	amfList := collections.GetAmfCollection()

	//Creation of request
	event1 := models.AmfEvent{Type: models.AmfEventType_PRESENCE_IN_AOI_REPORT, ImmediateFlag: true}
	eventList := &[]models.AmfEvent{event1}

	eventMode := &models.AmfEventMode{Trigger: models.AmfEventTrigger_ONE_TIME}

	eventSubscription := &models.AmfEventSubscription{EventList: eventList, NotifyCorrelationId: "1", NfId: context.NWDAF_Self().NfId,
		AnyUE: true, Options: eventMode}

	createEventSub := models.AmfCreateEventSubscription{Subscription: eventSubscription}

	//For each amf, we need to query amf events in order to obtain a list of connected UEs
	for amfId := range amfList {
		//Reset device count for every amf, new ones will be added.
		amfList[amfId].ConnectedUes = make([]*string, 0)
		externalAMFconfiguration := Namf_EventExposure.NewConfiguration()
		serviceList := amfList[amfId].Profile.NfServices
		var apiUri string

		for idxservice := range *serviceList {
			if (*serviceList)[idxservice].ServiceName == models.ServiceName_NAMF_EVTS {
				apiUri = (*serviceList)[idxservice].ApiPrefix
				break
			}
		}

		if apiUri != "" {
			externalAMFconfiguration.SetBasePath(apiUri)
			client := Namf_EventExposure.NewAPIClient(externalAMFconfiguration)

			createdSub, response, err := client.SubscriptionsCollectionDocumentApi.CreateSubscription(gocontext.TODO(), createEventSub)

			if err == nil {
				_ = response
				amfEventsReport := createdSub.ReportList

				//For every report we extract info about the UE and add it to the list in the AMF instance
				for i := range amfEventsReport {
					supi := amfEventsReport[i].Supi
					var amfInstance = amfList[amfId]
					amfInstance.ConnectedUes = append(amfInstance.ConnectedUes, &supi)
				}
				//We add the metric to the system in order to track the number of UEs connected to this AMF
				metricName := fmt.Sprintf("free5gc_amf_%s_connected_ues", amfList[amfId].CustomName)
				metrics.AddMetric(metrics.MetricElement{Name: metricName, Help: "Connected UEs to an AMF instance", Value: float64(len(amfEventsReport))})
			}
		}
	}
}

//GetDevicesInfoFromAfm
//Ask information for each device in the AMF instance list.
func GetDevicesInfoFromAfm(instanceAmf context.AmfInstance) []models.AmfEventReport {
	var eventReport = make([]models.AmfEventReport, 0)

	event1 := models.AmfEvent{Type: models.AmfEventType_CONNECTIVITY_STATE_REPORT, ImmediateFlag: true}

	event2 := models.AmfEvent{Type: models.AmfEventType_REACHABILITY_REPORT, ImmediateFlag: true}

	event3 := models.AmfEvent{Type: models.AmfEventType_LOCATION_REPORT, ImmediateFlag: true}

	event4 := models.AmfEvent{Type: models.AmfEventType_REGISTRATION_STATE_REPORT, ImmediateFlag: true}

	eventList := &[]models.AmfEvent{event1, event2, event3, event4}

	eventMode := &models.AmfEventMode{Trigger: models.AmfEventTrigger_ONE_TIME}

	serviceList := instanceAmf.Profile.NfServices
	var apiUri string

	//Search if NAMF_EVTS is in the service available from AMF
	for idxservice := range *serviceList {
		if (*serviceList)[idxservice].ServiceName == models.ServiceName_NAMF_EVTS {
			apiUri = (*serviceList)[idxservice].ApiPrefix
			break
		}
	}

	if apiUri != "" {
		//For every device in AMF connected device list asks for info and put it the eventReportList
		for device_idx := range instanceAmf.ConnectedUes {
			eventSubscription := &models.AmfEventSubscription{Supi: *instanceAmf.ConnectedUes[device_idx], EventList: eventList, NotifyCorrelationId: "1", NfId: context.NWDAF_Self().NfId, Options: eventMode}
			createEventSub := models.AmfCreateEventSubscription{Subscription: eventSubscription}
			externalAMFconfiguration := Namf_EventExposure.NewConfiguration()
			externalAMFconfiguration.SetBasePath(apiUri)
			client := Namf_EventExposure.NewAPIClient(externalAMFconfiguration)

			createdSub, response, err := client.SubscriptionsCollectionDocumentApi.CreateSubscription(gocontext.TODO(), createEventSub)

			if err == nil {
				_ = response //TODO
				eventReport = append(eventReport, createdSub.ReportList...)
			}
		}
	}
	return eventReport
}

// UpdateDevicesInfoAmfs
// For every AMF instance, it asks for every connected device (to an AMF instance) info in eventReport type, then it extracts
// information in each event report and put it inside the metric system.
func UpdateDevicesInfoAmfs() {
	globalConnStats := metric_elements.ConnectivityStats{}
	metric_elements.ResetGlobalConnStat(globalConnStats)
	globalConnStats.Entity = "core"
	globalRegistrData := metric_elements.RegistrationStats{}
	metric_elements.ResetGlobalRegistrStat(globalRegistrData)
	globalRegistrData.Entity = "core"
	amfList := collections.GetAmfCollection()

	for amfInstance := range amfList {
		amfDevInfo := GetDevicesInfoFromAfm(*collections.GetAmfCollection()[amfInstance])
		connStats := metric_elements.ConnectivityStats{}
		metric_elements.ResetGlobalConnStat(connStats)
		connStats.Entity = amfList[amfInstance].CustomName
		registrData := metric_elements.RegistrationStats{}
		metric_elements.ResetGlobalRegistrStat(registrData)
		registrData.Entity = amfList[amfInstance].CustomName

		//Extract, for each report, data for the metric system
		//TODO add other possible types of report
		for i := range amfDevInfo {
			switch amfDevInfo[i].Type {
			case models.AmfEventType_CONNECTIVITY_STATE_REPORT:
				updateConnectivityStateData(&connStats, amfDevInfo[i])
			case models.AmfEventType_LOCATION_REPORT:
				//TODO
			case models.AmfEventType_REGISTRATION_STATE_REPORT:
				updateRegistrationStateData(&registrData, amfDevInfo[i])
			default:
			}
		}

		AddToGlobalConnMetrics(&globalConnStats, &connStats)
		AddToGlobalRegrMetrics(&globalRegistrData, &registrData)
		//Add extracted data to the metric system
		metric_elements.InsertConnMetricInMap(connStats)
		metric_elements.InsertRegistationMetricInMap(registrData)
		//Location_report usa Location
	}
	metric_elements.InsertConnMetricInMap(globalConnStats)
	metric_elements.InsertRegistationMetricInMap(globalRegistrData)
}

func updateConnectivityStateData(stats *metric_elements.ConnectivityStats, report models.AmfEventReport) {
	if &report != nil {
		if report.CmInfoList != nil {
			for in := range report.CmInfoList {
				rep := report.CmInfoList[in]
				switch rep.CmState {
				case models.CmState_CONNECTED:
					if rep.AccessType == models.AccessType__3_GPP_ACCESS {
						stats.Connected3GPPUes++
					} else {
						stats.ConnectedNON3GPPUes++
					}
				case models.CmState_IDLE:
					if rep.AccessType == models.AccessType__3_GPP_ACCESS {
						stats.Idle3GPPUes++
					} else {
						stats.IdleNON3GPPUes++
					}
				default:

				}
			}
		}
	}
}

func updateRegistrationStateData(stats *metric_elements.RegistrationStats, report models.AmfEventReport) {
	if &report != nil {
		if report.RmInfoList != nil {
			for in := range report.RmInfoList {
				rep := report.RmInfoList[in]
				switch rep.RmState {
				case models.RmState_REGISTERED:
					if rep.AccessType == models.AccessType__3_GPP_ACCESS {
						stats.Registered3GPPUes++
					} else {
						stats.RegisteredNON3GPPUes++
					}
				case models.RmState_DEREGISTERED:
					if rep.AccessType == models.AccessType__3_GPP_ACCESS {
						stats.Deregistered3GPPUes++
					} else {
						stats.DeregisteredNON3GPPUes++
					}
				default:

				}
			}
		}
	}
}
