package dataCollectors

import (
	"fmt"
	"github.com/free5gc/openapi/Nsmf_EventExposure"
	"github.com/free5gc/openapi/models"
	"gitlab.com/Paolort/nwdaf/internal/context"
	gocontext "golang.org/x/net/context"
	"time"
)

//TODO change name
func GetDevicesInfoFromSmf(instanceSmf context.SmfInstance) models.NsmfEventExposure {
	var createdSub models.NsmfEventExposure

	//smfEventSubList := []models.EventSubscription{{Event: models.SmfEvent_AC_TY_CH}, {Event: models.SmfEvent_UE_IP_CH}, {Event: models.SmfEvent_UP_PATH_CH}, {Event: models.SmfEvent_PLMN_CH}, {Event: models.SmfEvent_PDU_SES_REL}}
	smfEventSubList := []models.EventSubscription{{Event: models.SmfEvent_PDU_SES_REL}}

	serviceList := instanceSmf.Profile.NfServices
	var apiUri string

	//Checking if there is the service in the service list of the specific SMF
	for idxservice := range *serviceList {
		if (*serviceList)[idxservice].ServiceName == models.ServiceName_NSMF_EVENT_EXPOSURE {
			apiUri = (*serviceList)[idxservice].ApiPrefix
			break
		}
	}

	if apiUri != "" {
		var actualTime time.Time = time.Now()
		actualTime.AddDate(0, 2, 2)
		var expire = &(actualTime)

		var eventUri = fmt.Sprintf("%s/smf_event_notification", context.NWDAF_Self().GetIPv4Uri())
		eventSubscription := models.NsmfEventExposure{NotifId: context.NWDAF_Self().NfId, NotifUri: eventUri, EventSubs: smfEventSubList, ImmeRep: true, NotifMethod: models.NotificationMethod_PERIODIC, MaxReportNbr: 1024,
			Expiry: expire, RepPeriod: 10}

		externalSmfConf := Nsmf_EventExposure.NewConfiguration()
		externalSmfConf.SetBasePath(apiUri)
		smfApiClient := Nsmf_EventExposure.NewAPIClient(externalSmfConf)
		createdSub2, response, err := smfApiClient.DefaultApi.SubscriptionsPost(gocontext.TODO(), eventSubscription)

		createdSub = createdSub2

		if err == nil {
			_ = response //TODO
		}
	}
	return createdSub
}
