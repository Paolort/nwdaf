package dataCollectors

import (
	"gitlab.com/Paolort/nwdaf/metrics/metric_elements"
)

func AddToGlobalRegrMetrics(globalStats *metric_elements.RegistrationStats, toAddStats *metric_elements.RegistrationStats) {
	globalStats.Registered3GPPUes += toAddStats.Registered3GPPUes
	globalStats.RegisteredNON3GPPUes += toAddStats.RegisteredNON3GPPUes
	globalStats.Deregistered3GPPUes += toAddStats.Deregistered3GPPUes
	globalStats.DeregisteredNON3GPPUes += toAddStats.Deregistered3GPPUes
}

func AddToGlobalConnMetrics(globalStats *metric_elements.ConnectivityStats, toAddStats *metric_elements.ConnectivityStats) {
	globalStats.Connected3GPPUes += toAddStats.Connected3GPPUes
	globalStats.Idle3GPPUes += toAddStats.Idle3GPPUes
	globalStats.ConnectedNON3GPPUes += toAddStats.ConnectedNON3GPPUes
	globalStats.IdleNON3GPPUes += toAddStats.IdleNON3GPPUes
}
