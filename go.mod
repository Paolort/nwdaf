module gitlab.com/Paolort/nwdaf

go 1.14

require (
	github.com/antonfisher/nested-logrus-formatter v1.3.1
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d
	github.com/free5gc/aper v1.0.4
	github.com/free5gc/nas v1.0.7
	github.com/free5gc/ngap v1.0.6
	github.com/free5gc/openapi v1.0.4
	github.com/free5gc/util v1.0.2
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.8.0
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/uuid v1.3.0
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/urfave/cli v1.22.5
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/net v0.0.0-20220531201128-c960675eff93
	gopkg.in/yaml.v2 v2.4.0
)
